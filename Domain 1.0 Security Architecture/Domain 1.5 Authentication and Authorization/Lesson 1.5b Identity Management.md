# Lesson 1.5b Identity Management

## Lab Goals/Summary
- In this lab we will create a couple different users, change their permissions, create groups, and then add users to that group.
- In this lab we will also change the password on a users account, and the `root` user.
- For this lab I am using a Debian-based Linux OS.

1. First we must open our Linux Terminal. 

![](/Images/kali%20terminal.png)

2. Now let's create some users and go over the different options. When we use just the `adduser` command we will get a walk-through of all the options we can add for the user.

`sudo adduser student01`

![](/Images/adduser%20command.png)

3. The `adduser` command has a ton of other options/switches to be used with it. 

```
-c, --comment COMMENT: Specifies a comment or description for the user.
-g, --gid GROUP: Specifies the initial primary group for the user. The group must already exist.
-s, --shell SHELL: Specifies the login shell for the user.
-d, --home DIR: Specifies the home directory for the user.
-m, --create-home: Creates the user's home directory if it doesn't exist.
-p, --password PASSWORD: Sets the password for the user. It's recommended to use the passwd command to set the password separately for security reasons.
-e, --expiredate EXPIRE: Specifies the expiration date for the user account.
```

4. Now lets use the `usermod` command to add a comment for a user. And then we will use the `finger` command to view the users profile.

`sudo usermod -c "FULL NAME" [username]`

![](/Images/usermod%20c%20.png)

`sudo finger student01`

![](/Images/finger%20command.png)

5. Now let's create a user, view the user, and then delete the user, using the `userdel` command. 

`sudo adduser student69`

`cat /etc/passwd | grep student69`

`sudo userdel student69`

![](/Images/userdel%20command.png)

6. Now lets create a new group on our system, and then add a user to that group, and then view that group with two different commands.

`sudo adduser student420`

`sudo groupadd sales`

`sudo usermod -aG sales student420`

`groups student420`

`cat /etc/groups | grep sales`

![](/Images/group%20command.png)

![](/Images/cat%20group.png)

7. Now we are going to use the `passwd` command to change the password for our `root` user.

`sudo passwd root`

![](/Images/passwd%20command.png)

