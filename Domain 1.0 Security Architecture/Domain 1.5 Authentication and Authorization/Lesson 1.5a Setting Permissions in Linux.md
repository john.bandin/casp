# Lesson 1.5a Command-line Permissions

![](/Images/linux%20permissions%20made%20easy.gif)

![](/Images/linux%20permissions.jpg)

## Lab Goals/Summary


- In this lab we will use the `chmod` command to configure permissions on our files, users and groups.
- In this lab we will also cover how to set permisions only for a user, group or other using the three basic permissions: read (r), write (w), and execute (x).
- We will also cover how to use the octal file permissions. 
- In this lab we will also cover how to be more granular with permissions by setting Access Control Lists.
- For this lab I am using a Debian-based Linux OS (Kali Linux)

1. First we will open our terminal.

![](/Images/kali%20terminal.png)

2. Now lets create three users, two groups and a couple files to use for this lab. I am running these commands from the users Desktop.

```
sudo useradd student10 && sudo useradd student20 && sudo useradd student30

sudo groupadd marketing && sudo groupadd accounting

sudo groupadd sales

touch file1 file2 file3
```

![](/Images/add%20multiple%20users.png)

![](/Images/add%20multiple%20files.png)

3. The first permissions we will set is making the owner of `file1` read and write permissions. First let's look at the owner of the file, and then change the owner to `student10`, and set the new owner to have "**read**" and "**write**" permissions.

`ls -la`

![](/Images/see%20ownership.png)

4. As we can see the owner is "johnny", now lets change the ownership and group using the `chown` command.

`sudo chown student10:sales /home/johnny/Desktop/file1`

`ls -la /home/johnny/Desktop/file1`

![](/Images/see%20permissions%20on%20file1.png)

5. Now lets set the owner to have "**read**" and "**write**" and "**execute**" permissions.

`sudo chmod u+rwx file1`

![](/Images/chmod%20u%2Brwx.png)

6. Notice the permissions change for the second column. We just changed permissions for the owner using this command `chmod u`. Check the photo below to see which columns correlate to the `owner`, `group` and `other` permissions. 

![](/Images/linux%20permissions.jpg)

7. Now lets set the `group` permissions to "**read**" and "**execute**" and set the `others` permissions to "**read**" and "**write**"".

`sudo chmod g+rx file1`

`sudo chmod o+rw file1`

![](/Images/chmod%20again.png)

8. Now lets set permissions for `file2` using numerical representations to set permissions. To give the owner read and write permissions, the group read and execute permissions, and others only execute permissions run the command below.

`sudo chmod 751 file2`

![](/Images/linux%20permissions%20made%20easy.gif)

9. Now let's set permissions using the `setfacl` command. First lets check out the permissions on "**file3**".

`sudo getfacl file3`

![](/Images/getfacl%20command.png)

10. Now, let's add a specific user "**student30**" with read, write, and execute permissions. Before we do this we will also change ownership of "**file3**" to "**student30**".

`sudo chown student30:sales file3`

![](/Images/chown%20again.png)

`sudo setfacl -m u:student30:rwx file3`

`sudo getfacl file3`

![](/Images/set%20facl%20command.png)

11. Now lets use the `setfacl` command to change the `groups` permissions to `read` and `execute`.

`sudo setfacl -m g:sales:rx file3`

![](/Images/set%20facl%20command%20again.png)

12. The last thing we will do is remove permissions using the facl command. The whole point of using the `setfacl` command is to set permissions for singular users and be granular in how you approach file permissions. So first let's set permissions on file3 for student 10, and then remove that ACL entry.

`sudo setfacl -m u:student10:rw file3`

`sudo getfacl file3`

![](/Images/setfacl%20command%203.png)

`sudo setfacl -x u:student10 file3`

![](/Images/remove%20facl.png)

