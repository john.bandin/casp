# Lesson 1.1 SIEM Configuration

## Lab Goals/Summary

## For this lab we will install an open-source SIEM and EDR software solution. We will be installing `wazuh`. This is a free and completely open-source solution for SIEM. Wazuh offers enterprise support, but we will be using it just for lab purporses. Wazuh allows us to see the deployment of a SIEM and also the three central components of an SIEM without having to spend thousands of dollars on a solution like splunk, to learn the fundamentals of SIEM. 

## Wazuh has three central components, the Wazuh indexer, the Wazuh Server, and the Wazuh agent. Wazuh can be deployed in a multi-cluster environment, but we will be installing the indexer, server and the web-based dashboard on a single  node due to hardware constraints.

- Get hands on learning for SIEM, EDR and HIDS software.
- Deploy a SIEM indexer, server and managed endpoints

1. First we will access an Ubuntu VM on our desktop, and open up the Bash terminal.

2. Next we will run the following commands to first start installing our SIEM Indexer.

```
# curl -sO https://packages.wazuh.com/4.5/wazuh-certs-tool.sh
# curl -sO https://packages.wazuh.com/4.5/config.yml
```

![](/Images/Screenshot%202023-09-12%20at%209.03.16%20PM.png)

3. Next we need to verify our IP address. Our IP address is going to set in the `config.yml` file as our indexer, server, and dashboard IP. Since we are doing a single node deployment.

```
ip a 
```
![](/Images/Screenshot%202023-09-12%20at%209.04.33%20PM.png)

4. Now lets use `nano` to edit our `config.yml` file. we need to edit our `./config.yml` and replace the node names and IP values with the corresponding names and IP addresses. You need to do this for all Wazuh server, Wazuh indexer, and Wazuh dashboard nodes. Add as many node fields as needed. For this example the IP address i am using is my VM's IP, which is, 172.16.158.129.

```
sudo nano ./config.yml
```

![](/Images/Screenshot%202023-09-12%20at%209.06.19%20PM.png)

![](/Images/Screenshot%202023-09-12%20at%209.06.42%20PM.png)

5. Once you have made the changes in the `./config.yml` file using nano, save the file by hitting the "Ctrl + O" and the "Ctrl + X" keys. 

6. Next we will create our certificates. These are the public/private key pairs needed to use SSL/TLS. Run the command below to create all the necessary keys. 

```
sudo bash ./wazuh-certs-tool.sh -A
```

![](/Images/Screenshot%202023-09-12%20at%209.12.15%20PM.png)

7. Now lets check all the contents we created using the `tree` command. We would need to compress all these contents if we had a multi-cluster setup. Just incase we want to do a multi-cluster setup later we will compress our certificates.

```
sudo tree wazuh-certificates
```

![](/Images/Screenshot%202023-09-12%20at%209.13.20%20PM.png)

```
sudo tar -cvf ./wazuh-certificates.tar -C ./wazuh-certificates/ .
```

![](/Images/Screenshot%202023-09-12%20at%209.22.11%20PM.png)



8. Now let's install our package dependencies. We may get a screen pop-up in our terminal telling us to restart services. Check all the boxes as depicted in the image below and click `ok`

```
sudo apt-get install debconf adduser procps
```

![](/Images/Screenshot%202023-09-12%20at%209.16.49%20PM.png)

9. You may need to log back in to your Ubuntu VM, once you do let's keep driving on with the installation process. Now lets Add the Wazuh repository. Follow these commands below.

```
sudo apt-get install gnupg apt-transport-https
```

![](/Images/Screenshot%202023-09-12%20at%209.23.23%20PM.png)

10. **NOTE A RESTART SERVICES SCREEN MAY APPEAR, JUST CLICK ENTER TO RESTART THE DEFAULT SERVICES**. 

11. Now let's Install the GPG key.

```
sudo su

curl -s https://packages.wazuh.com/key/GPG-KEY-WAZUH | gpg --no-default-keyring --keyring gnupg-ring:/usr/share/keyrings/wazuh.gpg --import && chmod 644 /usr/share/keyrings/wazuh.gpg
```

![](/Images/Screenshot%202023-09-12%20at%209.27.36%20PM.png)

12. Now, let's Add the repository.

```
echo "deb [signed-by=/usr/share/keyrings/wazuh.gpg] https://packages.wazuh.com/4.x/apt/ stable main" | tee -a /etc/apt/sources.list.d/wazuh.list
```

![](/Images/Screenshot%202023-09-12%20at%209.29.35%20PM.png)

13. Now lets do a final update on our system.

```
apt-get update
```

![](/Images/Screenshot%202023-09-12%20at%209.30.24%20PM.png)

14. Now lets actually install the `wazuh indexer`

```
apt-get -y install wazuh-indexer
```

![](/Images/wazuh%20apt%20installer.png)

15. Now let's configure the wazuh indexer. We will need to know the name of our indexer, which can be found in the `config.yml` file. If you have been following guide our indexer node name is `node-1`. We will also have to know the IP adddress of our `wazuh indexer`.

16. First, we will edit the `/etc/wazuh-indexer/opensearch.yml` configuration file and replace the following values:
 

- `network.host`: Sets the address of this node for both HTTP and transport traffic. The node will bind to this address and use it as its publish address. Accepts an IP address or a hostname. Use the same node address set in `config.yml` to create the SSL certificates.

- `node.name`: Name of the Wazuh indexer node as defined in the `config.yml` file. For example, `node-1`


- `cluster.initial_master_nodes`: List of the names of the master-eligible nodes. These names are defined in the `config.yml` file. Uncomment the node-2 and node-3 lines, change the names, or add more lines, according to your config.yml definitions

- `plugins.security.nodes_dn`: List of the Distinguished Names of the certificates of all the Wazuh indexer cluster nodes. Uncomment the lines for `node-2` and `node-3` and change the common names (CN) and values according to your settings and your config.yml definitions.

17. Use nano to edit the following file. 

```
nano /etc/wazuh-indexer/opensearch.yml
```

![](/Images/opensearcherfile.png)

18. Now we are going to install the certificate. First we will set our `NODE_NAME` variable.

```
NODE_NAME=node-1
```

![](/Images/node%20name%201.png)

19. Now let's run these set of commands below to install our SSL certificate.

```
mkdir /etc/wazuh-indexer/certs
tar -xf ./wazuh-certificates.tar -C /etc/wazuh-indexer/certs/ ./$NODE_NAME.pem ./$NODE_NAME-key.pem ./admin.pem ./admin-key.pem ./root-ca.pem
mv -n /etc/wazuh-indexer/certs/$NODE_NAME.pem /etc/wazuh-indexer/certs/indexer.pem
mv -n /etc/wazuh-indexer/certs/$NODE_NAME-key.pem /etc/wazuh-indexer/certs/indexer-key.pem
chmod 500 /etc/wazuh-indexer/certs
chmod 400 /etc/wazuh-indexer/certs/*
chown -R wazuh-indexer:wazuh-indexer /etc/wazuh-indexer/certs
```

![](/Images/indexer%20ssl%20cert.png)

20. Now we are going to start the `indexer` service using `systemctl`

```
systemctl daemon-reload
systemctl enable wazuh-indexer
systemctl start wazuh-indexer
```

![](/Images/indexer%20daemon.png)

![]()