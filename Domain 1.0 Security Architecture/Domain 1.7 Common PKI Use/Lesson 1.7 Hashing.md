# Lesson 1.7 Hashing Lab

## Lab Goals/Summary

- Verify integrity of a installation package using hashing. We will verify message digests/checksum from the Linux terminal.
- For this lab I am using a Debian-based OS (Kali Linux)


1. Open/start your Linux machine.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/00dca7a3-c88c-47f9-97ae-8f2f98c874bf/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=1668,817&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=811,584)


2. Open up a web browser. On my machine the web browser we will use is firefox.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/0f68572b-ca33-492e-aca8-2583ee052804/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=1651,1103&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=185,7)


3. On your web browser click in the URL to search for "**notepad++**". We are going to download this software and the sha256 binary package associated with our installation package.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/5f7daaf5-1ec7-44af-b7e1-191bf020d347/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=1667,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=371,66)


4.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/5e748acf-f3c3-430e-b215-7d2dbae2c9cf/user_cropped_screenshot.jpeg?tl_px=0,118&br_px=1679,1079&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=198,284)


5.Click the first "**downloads**" link. For this step it really doesn't matter which one we download. But for this Guide I am going to click and download "**notepad++ v8.5.6**".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/fec50cd6-e3c2-4c35-9cdb-4d0b13e93126/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=1645,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=523,275)


6. Now click the green "**download**" button to download the notepad++ package. Before we run the installer we want to verify that this package has integrity. We do this by verifying the checksum of downloaded package, to the checksum posted on the website.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/4d79cf5e-baa4-4829-b28f-c0ac78d49d50/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=1664,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=484,262)


7. Scroll down, and click the "**SHA-256 digests**" link. This will download a text file of all the checksums for all the different installation versions for notepad++ v8.5.6

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/56cc00d6-cca5-4681-a79d-4f271cc59b5a/user_cropped_screenshot.jpeg?tl_px=102,91&br_px=1478,860&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,277)


8. Now lets open up our bash Terminal. From the CLI we will verify the checksum of our downloaded package, and compare it to the public hash thats posted on the website. This public hash/checksum is in the "**sha-256 binaries**" file we download.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/00e81a1c-9df4-43ec-81bf-cd40b3ad94eb/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=1668,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=205,15)


![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/a43a9928-bd5d-43df-bd72-595a651ca575/ascreenshot.jpeg?tl_px=1841,195&br_px=2988,836&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,277)


10. From the terminal change into the `Downloads` directory.

`cd /home/[username]/Downloads`


11. Now lets take a look at our files in the in our "\*\*downloads\*\*" directory.

`ls -la`

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/711bdebf-f1eb-49a1-8a5d-6e7e5ea9a4d6/screenshot.png?tl_px=0,0&br_px=882,244&force_format=png&width=983)


12. Now lets take a look at our "SHA-256 Binaries" file. This file will show us all the corresponding checksums for all the possible notepad++ v8.5.6 packages.

`cat npp.8.5.6.checksums.sha256`


13. Now lets create a sha256 checksum from the CLI for our notepad++ installer.

`sha256sum npp.8.5.6.Installer.x64.exe `

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/db40dbe0-e31b-4ebb-ba98-43bf55d88fcf/screenshot.png?tl_px=0,0&br_px=797,77&force_format=png&width=860)


14. Now lets create a file using nano and name it "checkhash.txt". In this file we are going to copy and paste both checksums. The checksum from our "**sha256 binaries**" file, and the checksum we created by running the SHA256 hashing algorithm against our notepad++ installer.



1.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/48579626-ce1b-47d9-8d74-74444fd6c0f9/user_cropped_screenshot.jpeg?tl_px=0,95&br_px=1029,736&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=743,312)


2. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/3ee395cc-654c-40a7-aed5-a7b7d533f0d3/user_cropped_screenshot.jpeg?tl_px=0,39&br_px=1032,808&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=759,381)


3. `nano checkhash.txt`


![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/d43e42ef-abee-4803-a17b-301da1776744/ascreenshot.jpeg?tl_px=1682,263&br_px=2542,744&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/db20481c-b1aa-43f1-aba1-4543689b9581/ascreenshot.jpeg?tl_px=1779,329&br_px=2639,810&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


6. Now click `Ctrl + o` to save and then exit the nano editor. Now let's copy and paste the checksum we created. 



![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/bfe8c2a5-ea86-4c8c-afe8-67b2a9dc274b/user_cropped_screenshot.jpeg?tl_px=0,43&br_px=1030,813&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=756,475)



![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/524bfe94-b8ec-4d13-8f38-a2606e121ef2/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=1035,640&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=747,160)


9. Now let's go back into our our "checkhash.txt" file using nano and paste the checksum, and compare the two values.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/0e8180f4-cfbb-474d-b8b7-03b6ea6b6a51/ascreenshot.jpeg?tl_px=1234,235&br_px=2954,1196&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,276)


![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-08-29/7b481528-ecd0-4488-a0aa-fa553203abe6/ascreenshot.jpeg?tl_px=1801,190&br_px=2661,671&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)
#### [Made with Scribe](https://scribehow.com/shared/VMware_Fusion_Workflow__qnkJY0UJSdmkSql0u8AzTA)

![](/Images/nano%20screenshot%209.png)

10. Now as we can see from the image above both of the checksums are the same. This verifies that our installation package has integrity and that it is safe to install.




