# Lesson 1.3 Version Control

## Lab Goals/Summary

- In this lab we are going to learn the fundamental concepts of version control using Git on a Linux System. 
- In this lab we will also create a GitHub account and push our local repository.
- For this lab I am using a Debian-based Linux OS. (kali linux).

1. First open up your Linux terminal. 

![](/Images/kali%20terminal.png)

2. Next we will update our repository using the `apt` command and install `git`. **NOTE ON MOST LINUX DISTROS GIT IS ALREADY INSTALLED**

```
sudo apt update
sudo apt install git
```

![](/Images/install%20git.png)

3. Now let's set up our `git` global `user.name` and `user.email`. **MAKE SURE THAT THIS IS GOING TO BE THE SAME EMAIL YOU USE TO CREATE YOUR GITHUB ACCOUNT**

```
git config --global user.name johnbandinit@gmail.com

git config --global user.email johnbandinit@gmail.com
```

![](/Images/git%20global.png)

4. Now that we have installed GIT, let's create our repository. We will use the `mkdir` command to create a new directory. **NOTE I AM CREATING THIS DIRECTORY UNDER MY USERS HOME FOLDER**

```
sudo mkdir /home/johnny/git-lab

cd /home/johnny/git-lab
```

![](/Images/mkdir%20git.png)

5. Now let's initilize our new repo using `git` commands. We will do this by using the `git init` command.

```
git init
```

![](/Images/git%20init%20command.png)



6. Next we have to create a file in our new repository so we can see the full functionality of Git as a Source Control Manager. Create a file named "**app.py**", and add the code below to the file. We will create this file using the `nano` command. 

```
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello, this is a simple website!"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
```

```
sudo nano app.py
```

```
Ctrl + o

Ctrl + x
```

![](/Images/nano%20app%20pythong.png)

7. Now that we have created our file let's check the status of our repo. We need to see what files are committed, which aren't and to see which files are staged to be committed. If we do not commit our files, they will not be a part of our Git Repository. We will check the status of our Repo using the `git status` command.

```
sudo git status
```

![](/Images/git%20status.png)

8. As you can see from the command above, we have "untracked" files. We will need to "**add**" these files and then commmit the files. Let's add our new files to be "tracked" or "staged" and then let's commit the files.

```
git add app.py

git status
```

![](/Images/git%20status%20from%20add.png)

9. Now that we have added the files, let's commit these files. When we commit a file we have to add a message to our "**commit**". We will use the `git commit` command to commit our files to Git.

```
git commit -m "Adding our python file"

git status
```

![](/Images/git%20commit.png)

10. Now that we have committed our files to the git repository, lets go over different branches. We have committed our files on the "**master**" branch. If we were working in a team, and we want our team to make or suggest changes to our Repository, we can create "**branches**" to our "**master**" branch, and make changes in that forked "**branch**" without affecting the "**master**" branch. Let's create a new "**branch**" called `development`. And then move to that new branch, and check what branch we are in.

```
git branch development

git checkout development

git branch
```

![](/Images/git%20branch%20command.png)

11. As we can see our Git repo now has two branches, and we are currently in the "**development**" branch. Now let's create a new file in the "**development**" branch, and then let's merge our "**development**" branch into the master branch.

```
echo "this is a feature." > feature.txt

git add feature.txt

git commit -m "added a new feature"

```

![](/Images/git%20branch%20feature.png)

12. now that are files are committed, let's switch back to the "**master**" branch and merge our "**development**" branch into our "**master**" branch. 

```
git checkout master

git merge development
```

![](/Images/git%20merge%20feature.png)

13. Now let's checkout out our "**git**" log, list our branches, and then delete our "**development**" branch. 

```
git log

git branch -a

git branch -d development
```

![](/Images/git%20log%20and%20delete%20branch.png)

14. Now that we have gone through commands on the terminal, let's upload our Git Repository to GitHub. Follow this Guide to create your GitHub account.

**[Create a GitHub Account](https://scribehow.com/shared/Step-by-Step_Guide_to_Creating_a_GitHub_Account__2sxe7ot3Q-GqS6Nqs4lnKg
)**

15. Now we're going to configure some global settting for Git and then upload our local repository to our GitHub account.

```
git config --global user.name "amandasnewnew"

git config --global user.email amanda.aldecoa@trepatech.com

```

![](/Images/git%20config%20global.png)

16. Now let's push our repository to our GitHub repo online. First we will create a new repo online. Follow these steps below.

![](/Images/new%20repo%20on%20github.png)

![](/Images/github%20new%20repo.png)

17. Now lets copy the URL of our new repo on GitHub. 

`https://github.com/amandasnewnew/git-lab`

18. Now we're going to push the local repo using the commands below. Before we do work with github, we will need to create a personal access token. Use the URL below to create a personal access token. Make sure you save that personal access token, it will be used as your password.

**[Create Personal Access Token](https://scribehow.com/shared/Guide_Generating_a_Personal_Access_Token_with_Specific_Permissions__JVBpj8VYR12ZlagX2g2pDQ)**



```
git remote add origin https://github.com/amandasnewnew/git-lab

git remote -v

git push origin master

```

![](/Images/git%20add%20origin.png)

![](/Images/git%20push.png)

19. Now lets check our GitHub and see if our repo was pushed!

![](/Images/github%20check.png)








