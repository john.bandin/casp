# Lesson 1.2a Containers

![](/Images/Linux%20Image%20(8).png)

## Lab Goals/Summary

- In this lab we will download docker and go over the basics of containerization, and how to do basic work within docker.
- For this lab I am using a Debian-based Linux OS. (kali linux)

1. First open up your Linux terminal. 

![](/Images/kali%20terminal.png)

2. Now we must install docker on our Linux Machine. We will use the `apt` package installer to update our machine and install "**docker**"

```
sudo apt update
sudo apt install docker.io
```

![](/Images/install%20docker.png)

3. Next we will use the `systemctl` command to enable docker on boot and make it a "**daemon**". We will also verify that "**docker**" is running using the `systemctl` command.

```
sudo systemctl start docker
sudo sytemctl enable docker
```

![](/Images/systemctl%20enable%20docker.png)

```
sudo systemctl status docker
```

![](/Images/status%20docker.png)

4. Now we are going to run our first example container. We do this by running "**docker**" commands. Like the `docker run` commmand. We are going to run an ubuntu container that will give us a virtual/containerized ubuntu environment. Then we will run another "**docker**" command to list all the running containers on our "**docker**" engine.

```
sudo docker run -it ubuntu bash
```

![](/Images/docker%20ubuntu.png)

5. The command `docker run` will first look for a local repository of the container on the system, if it cannot find the container locally, it will look for the container in the global library and pull the container down. Notice there is also supplemental information about the container like the hash `checksum` where you can verify integrity. 

6. Notice from the image above we are now in a whole new "**bash**" environment. We are now in our new downloaded Ubuntu container. Let's open up a whole new terminal on our original Kali Linux VM and run our `docker ps` command to list our active running containers.

```
sudo docker ps
```

![](/Images/docker%20ps.png)

7. Notice from this output all the information we can see about our container. Now let's stop the container, and then remove the container. We will need to have the information posted in the image above to accomplish these tasks. To stop a container we use the `docker stop` command.

```
sudo docker stop [container_id or name]

sudo docker ps
```

![](/Images/stop%20docker%20.png)

8. Now we can see from above that our container is stopped. Now let's list our inactive containers, and then completely remove our ubuntu container.

```
sudo docker ps -a --filter "status=exited"

sudo docker rm [container_id or name]

```

![](/Images/rm%20container.png)

9. Now lets do something a bit more advanced, lets create, run and manage our own container. First lets create a new directory using the `mkdir` command, and then `cd` into that new directory. I am going to make this directory at the users home directory.

```
sudo mkdir /home/johnny/my-docker-app

cd /home/johnny/my-docker-app

pwd
```

![](/Images/mkdir%20container.png)

10. Now inside our containers directory we are going to make a simple web application named `app.py`. This is going to a python based web server, using the "**flask**" framework. We are going to use `nano` to create the file, and then paste this code below into your `app.py` file.

```
# app.py
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
    return "Hello, Docker!"

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
```

```
sudo nano app.py
```

![](/Images/nano%20app.png)

```
Ctrl + o

Ctrl + x
```

11. Now lets create a docker file in the same directory. Using `nano` create file name `Dockerfile`. Note that this file does not have a specific file extension. Paste this code below into your `Dockerfile`.

```
# Dockerfile
FROM python:3.8-slim-buster
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["python", "app.py"]
```

```
sudo nano Dockerfile
```
![](/Images/dockerfile.png)

```
Ctrl + o

Ctrl + x
```

12. Now we need to create a `requirements.txt` file that will contain all the python libraries we will need to import for our web application to work. We will we use `nano` to create our `requirements.txt` file. Paste this code below in your `requirements.txt` file.

```
Flask==2.1.1
```

```
nano requirements.txt
```

![](/Images/flask.png)

```
Ctrl + o

Ctrl + x
```

13. Now, using our `Dockerfile` we will build our container. We build our container using the `docker build` command.

```
sudo docker build -t my-docker-app .
```

![](/Images/docker%20build%20command.png)

14. Now let's run the custom container using the `docker run` command.

```
sudo docker run -p 5000:5000 my-docker-app
```

![](/Images/run%20container.png)

15. Now that our custom webserver container is running, lets open up firefox, or your web browser of choice and see our new website! Paste this URL into the URL bar. 

`http://localhost:5000`

![](/Images/helllo%20docker.png)

16. Now to finish this lab, open up a new terminal and lets list our active custom container using the `docker ps` command.

```
sudo docker ps

```

![](/Images/docker%20ps%20again.png)








