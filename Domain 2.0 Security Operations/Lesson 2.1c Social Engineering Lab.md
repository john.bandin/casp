# Social Engineering lab on Kali Linux

## Summary

In this lab, we will walk through the steps of using the **setoolkit** on a Kali Linux VM. This tool allows us to create Social Engineering tools like phishing emails, or credential harvesting websites. For this lab we will also use the OWASP Broken Authentication VM. This is a VM that can be deployed on VMWare player and the whole purpose of this VM is to host a website that can be spoofed, and has broken access control. Essentially this VM is meant to be used for lab-based walkthroughs of SE attacks and Web Application attacks. 

## Prerequisites

- Kali linux VM or Device
- Root level access to the Kali Linux machine
- OWASP Broken Web Application VM on local network


## Step 0: Copy the OWASP VM Zip File from the Local Network Share, and unzip the folder. (Skip if VM Is already installed)

![](/Images/Screenshot%202023-10-15%20at%2011.32.13%20AM.png)

**`If your Type 2 Hypervisor has both of the underlined VMs in the image above skip this step`**

In this step you will need to map to the local share drive, copy the OWASP VM Zipped folder and paste it into your local `downloads` folder. Then you will need to extract all the files by `unzipping` the folder. You do this by `right-clicking` and then clicking `extract-all`.

## Step 1: Make sure that the OWASP VM is installed on your Type 2 Hypervisor (VMWare Player) (Skip this step if OWASP VM is already Functional and working)

![](/Images/Screenshot%202023-10-15%20at%2011.34.51%20AM.png)

**`IF your OWASP VM is working and looks like this image above take note of the IP address and move onto step 4`**

If the VM is not installed checkout this How To for instructions on how to deploy a VM on VMware player.

[How To Install OWASP VM](https://scribehow.com/shared/How_to_Install_VMware_Workstation_Pro_on_Windows__OzmYUGqsSxu4yvIW06ojrA)

## Step 2: Login to your OWASP VM and make sure that the website we will be spoofing is working.

If you do not know how to do this check out our Instruction here: (you can skip the sections going over installation)

[Checking Our OWASP VM](https://scribehow.com/shared/How_to_Install_VMware_Workstation_Pro_on_Windows__OzmYUGqsSxu4yvIW06ojrA)


## Step 3: Login into your kali Linux Machine

```
Username: kali

Password: kali 
```

![](/Images/kali%20linux%20login.png)

## Step 4: Open up our OWASP VM Webpage

Now we have logged into our Kali Linux Machine, let's make sure we can reach our OWASP VM Website that we will be spoofing. In this Guide the OWASP VM can be reached at `192.168.128.132`

![](/Images/Screenshot%202023-10-15%20at%2011.34.51%20AM.png)

Open up firefox and in the URL header type your OWASP VM's IP Address using `http`


![](/Images/Screenshot%202023-10-13%20at%201.01.18%20PM.png)

Next we will click  on the `OWASP Mutillidae II` link.

![](/Images/Screenshot%202023-10-13%20at%201.04.34%20PM.png)

Then we will click **login/register**. This is the link that we will be spoofing.

![](/Images/Screenshot%202023-10-13%20at%201.05.25%20PM.png)

## Step 5: Run the `setoolkit`

Now lets open up our Kali Linux terminal and go through the steps to launching the `setoolkit`. First we will need to know our IP address. Run the following command from the terminal.

```
ip a
```

![](/Images/Screenshot%202023-10-13%20at%201.08.47%20PM.png)

Next we will need install or make sure that the `apache` web service is running. Run this command.

```
sudo systemctl start apache2
```

To make sure that it is running, type your Kali Linux Machine's IP address into the URL bar using `http`.

![](/Images/Screenshot%202023-10-13%20at%201.31.05%20PM.png)

![](/Images/Screenshot%202023-10-13%20at%201.31.39%20PM.png)

Now lets run the setoolkit command on our bash terminal.

```
sudo setoolkit
```

![](/Images/Screenshot%202023-10-13%20at%201.37.48%20PM.png)

Hit `y` and this will bring you to the menu. 

![](/Images/Screenshot%202023-10-13%20at%201.44.53%20PM.png)

## Step 6: Perform the credentail harvesting attack

Now we will walk through using the `setoolkit` to perform a credential harvesting attack.

Enter the number `1` for **Social-Engineering Attacks**

![](/Images/Screenshot%202023-10-13%20at%201.44.53%20PM.png)

Now we will enter the number `2` for **Website Attack Vectors**

![](/Images/Screenshot%202023-10-13%20at%201.49.53%20PM.png)

Now we will enter the number `3` for **Credential Harvester Attack Method**

![](/Images/Screenshot%202023-10-13%20at%201.50.58%20PM.png)

Then we will enter the number `2` for **site cloner**. 

![](/Images/Screenshot%202023-10-13%20at%201.51.46%20PM.png)

This will bring up an option to select the IP address to sent the HTTP POST Requests. We will just click inside the terminal and hit the `enter` key.

![](/Images/Screenshot%202023-10-13%20at%201.53.26%20PM.png)

next we will have to grab the URL we want to clone. Go back to firefox and copy the OWASP VM Login page's full URL and paste it back into the bash terminal.

![](/Images/Screenshot%202023-10-13%20at%201.55.37%20PM.png)

![](/Images/Screenshot%202023-10-13%20at%201.57.09%20PM.png)

Once the URL is pasted hit the `enter` key. Next you will have type in `y` and hit enter to restart the apache service. The picture below shows how your terminal should look. 

![](/Images/Screenshot%202023-10-13%20at%201.59.07%20PM.png)

## Step 7: DNS Poisoning Simulation

Now we will simulate DNS poisoing so the user get redirected to our cloned site, and we can steal the credentials. We will do this by changing the hosts file on our local windows machine. 

Open the `notepad` application as an admin, click `file-->open` and then paste this  path into the URL bar.

```
C:\Windows\System32\drivers\etc
```

![](/Images/Screenshot%202023-10-13%20at%202.11.36%20PM.png)

![](/Images/Screenshot%202023-10-13%20at%202.12.10%20PM.png)

![](/Images/Screenshot%202023-10-13%20at%202.12.44%20PM.png)



Make sure you select `all files` from the drop down menu. And then open the `hosts` file. This is the local DNS file that performs ip-->name resolution. Type in the following IP address mapping and ensure it matches the IP addresses on your Kali Linux VM. Ensure you save the hosts file!



```
192.168.128.131 trepatech.com

# credential-harvester-machine-ip spoofed-domain-name

```
![](/Images/Screenshot%202023-10-13%20at%202.14.27%20PM.png)

![](/Images/Screenshot%202023-10-13%20at%202.15.13%20PM.png)

![](/Images/Screenshot%202023-10-13%20at%202.15.40%20PM.png)

![](/Images/Screenshot%202023-10-13%20at%202.16.20%20PM.png)

![](/Images/Screenshot%202023-10-13%20at%202.16.48%20PM.png)


Then we will open our web browser and type in `trepatech.com` into the URL. Hit `enter` and we will see that we are now going to our spoofed website!

![](/Images/Screenshot%202023-10-13%20at%202.17.27%20PM.png)