# Lesson 2.1a Network Monitoring and Packet Capture

![](/Images/Linux%20Image.png)

## Lab Goals/Summary

- We will use CLI commands to view our Local network and TCP connections
- We will cover how to do packet capture and how to execute a NMAP scan.
- For this lab I am using a Debian-based OS (kali linux)


1. First we will need to open up a terminal on our machine.

![](/Images/kali%20terminal.png)

2. Now let's run the `ping` command and add some switches to our pings. 

```

ping 8.8.8.8 
ping 8.8.8.8 -s [byte size] # This switch lets us change the ICMP size
ping 8.8.8.8 -c # This switch lets set the number of pings.

```

![](/Images/ping%20test%201.png)

![](/Images/ping%20command%202.png)

3. Now let's run another networking utility that will allow us to see every single hop on the path of a packet. We will execute the `traceroute` command to our "**trepatech.com**" server. First we will use `nslookup` to find the IP address of our server.

```
nslookup trepatech.com
traceroute [X.X.X.X]
```

![](/Images/traceroute%20and%20nslookup.png)

4. Next let's take a look at all our active connections. We will first open up "**mozilla firefox**", and run a "**ping**" to google DNS. Then we will execute the `netstat` command without any additional switches.

`netstat`

![](/Images/netstat%20cmd.png)

5. As we can see from the `netstat` output, their is a lot of information being shown, we can use switches to filter the output. Below are just some useful switches.

```
netstat -a or netstat --all: Shows all active connections and listening ports, both TCP and UDP.

netstat -t or netstat --tcp: Displays only TCP connections and related information.

netstat -u or netstat --udp: Shows only UDP connections and related information.

netstat -n or netstat --numeric: Displays numerical addresses instead of resolving hostnames to IPs.

netstat -p or netstat --program: Shows the PID and program name associated with each network connection. Useful for identifying which process is using a particular connection.

netstat -r or netstat --route: Displays the kernel routing table, including information about the default gateway and other routes.

netstat -i or netstat --interfaces: Provides information about network interfaces, including statistics like packets and errors.
```
![](/Images/netstat%20antp.png)

6. The combination of `-antp` is one of the switches i use to see just the important information. Next we will execute the `lsof` command with some additional switches so we can generate only information for active network connections

`lsof -i`

![](/Images/lsof%20-i%20command.png)

7. As you can see from the image above the `-i` switch shows much less output than the regular `lsof` command. Now lets go over network monitoring using tcpdump/wireshark and NMAP.

8. First we will download wireshark. For debian-based Linux OS's run the following command to install wireshark or follow this How-To. https://www.geeksforgeeks.org/how-to-install-and-use-wireshark-on-ubuntu-linux/


9. `sudo add-apt-repository ppa:wireshark-dev/stable`

![](/Images/wireshark%201.png)

10. `sudo apt update`

11. `sudo apt install wireshark`

![](/Images/wireshark%202.png)

12. If you have issues with installing wireshark please inform the instructor. Now lets run a packet capture on the active interface. First we must determine the interface name/ID. 

`ip a`

![](/Images/ip%20a%202.png)

13. My interface name is going to "**Eth0**", we can see that from the photo above by running the `ip a` command. Next lets run our tcpdump utility with the following switches.

`sudo tcpdump -i eth0 -w capture-results.pcap -v`

![](/Images/tcpdump%20command.png)

14. Let the command run for about 15-20 seconds. The hit "Ctrl + c" on the keyboard to abort the "**tcpdump**". Now we can also see from above that we have created a .pcap file. We will view this file using wireshark. I will move this file from my `home` directory to the desktop. And then we will open up wireshark and open our `capture-results.pcap` file. 

`mv /capture-results.pcap /home/[username]/Desktop`

![](/Images/mv%20pcap.png)

15. Now we will open up our pcap file. Follow the photos below. Once the pcap file is open inspect a ICMP file.

![](/Images/wireshark%20photo1.png)

![](/Images/wireshark%20photo%202.png)

![](/Images/wireshark%20photo%203.png)

16. Now that we have done a packet capture let's do some network monitoring on our entire LAN using NMAP. NMAP has a LOT of options for monitoring the network. For this lab we will run a simple network scan.

`sudo nmap -Pn [YOUR_SUBNET]`

![](/Images/nmap%20scan%20cmd.png)

# Lab Summary

- In this lab we covered different networking CLI utilities to discover open ports and service in our LAN, inspect and view traffic coming from our local machine, and also how to identify active TCP connections from the CLI.
