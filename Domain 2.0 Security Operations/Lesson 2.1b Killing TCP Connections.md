# Lesson 2.1b Killing TCP Connections

![](/Images/Linux%20Image%20(11).png)

## Lab Goals/Summary

- create a daemon that starts a web browser.
- Kill the service and disable the systemctl

1. Open a linux terminal using "ctrl + alt + t"

![](/Images/ubuntu%20terminal.png)

2. Your raspberry PI should have `chromium` installed by default, but if not run the `apt` package installer to install chromium.

```
sudo apt-get update

sudo apt-get install chromium-browser # Ubuntu

sudo apt install chromium # Kali
```

2. If you are using a raspberry PI we will have to install the kiosk packages.

```
sudo apt update

sudo apt install xdotool unclutter
```

2. Now lets create a startup service (daemon) that opens a network session. We will use nano to create the `chromium-autostart.service`. This service will be save in the following directory. `/etc/systemd/system`, saving in this directory will make this file a daemon. Paste the following code below into our `chromium-autostart.service` file.

```
sudo su

cd /etc/systemd/system

nano chromium-autostart.service
```

![](/Images/chromium%20autostart.png)

```
[Unit]
Description=Chromium Autostart
After=graphical.target

[Service]
Environment=DISPLAY=:0
ExecStart=/usr/bin/chromium-browser --kiosk https://youtube.com 
# This is for ubuntu
ExecStart=/usr/bin/chromium --kiosk https://youtube.com 
# This is for Kali
User=YOUR_USERNAME
# Replace YOUR_USERNAME with your actual username

[Install]
WantedBy=graphical.target

```

![](/Images/daemon%20chromium.png)

4. Now we are going to set this service as a daemon. We will use the `systemctl` command to first `enable` and then `start` this service. 

```
sudo systemctl enable chromium-autostart.service

sudo systemctl start chromium-autostart.service

```

![](/Images/daemon%20start%20and%20enable.png)

5. Once this daemon is started Youtube.com will be loaded in `kiosk` mode. This means you will not be able to access any part of your Linux system. We need to kill this TCP connection and disable the `daemon` service, and then reboot the system to get out of `kiosk` mode. First open up a terminal by pressing the `ctrl + alt + t` keys. Once there, lets become a root user and run some commands to kill the TCP connection and disable the .service.

```
sudo su

netstat -antp
```

![](/Images/netstat%20antp%20command.png)

6. As we can see from our output we need to kill the chromium process. We do this by noting the Proccess ID or PID. The PID in my image is `4244`, yours will be different. Now let's use the `kill` command to kill the TCP connection. Then we will take a look at our `systemd` directory and use the `grep` command to filter the output, and just show us any `.service` files. We will locate the `chromium-autostart.service` and disable this service.

```
kill -9 4244

ls /etc/systemd/system | grep .service
```

![](/Images/find%20service.png)

7. Now that we located our service, let's disable our service using the `systemctl` command.

```
systemctl disable chromium-autostart.service

systemctl status chromium-autostart.service
```

![](/Images/systemctl%20chromium.png)

8. Now let's reboot our machine and ensure that chromium does not pop up again.

```
reboot
```


